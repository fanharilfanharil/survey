<?php

use App\Http\Controllers\API\JawabanController;
use App\Http\Controllers\API\PertanyaanController;
use App\Http\Controllers\API\SatkerController;
use App\Http\Controllers\API\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::middleware('auth:sanctum')->group(function(){
    // Route::get('user',[UserController::class, 'fetch']);
    Route::post('user',[UserController::class, 'updateProfile']);
    Route::post('user/photo',[UserController::class, 'updatePhoto']);
    Route::post('logout',[UserController::class, 'logout']);
});
Route::get('user',[UserController::class, 'all']);
Route::post('login',[UserController::class, 'login']);
Route::post('register',[UserController::class, 'register']);
Route::get('satker',[SatkerController::class, 'all']);
Route::post('satker',[SatkerController::class, 'tambah']);
Route::get('pertanyaan',[PertanyaanController::class, 'all']);
Route::post('pertanyaan',[PertanyaanController::class, 'tambah']);
Route::get('jawaban',[JawabanController::class, 'all']);
Route::post('jawaban',[JawabanController::class, 'tambah']);

Route::get('/question', 'App\Http\Controllers\SurveyController@getPertanyaan');
Route::post('/save-question', 'App\Http\Controllers\SurveyController@saveAnswer');
