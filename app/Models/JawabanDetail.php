<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JawabanDetail extends Model
{
    use HasFactory;

    protected $table = 'jawaban_detail';
    protected $fillable = [
        'id_jawaban', 'pertanyaan_id', 'jawaban' 
    ];
}
