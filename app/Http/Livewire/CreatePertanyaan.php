<?php

namespace App\Http\Livewire;

use App\Models\Pertanyaan;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class CreatePertanyaan extends Component
{
    public $pertanyaan;
    public $pertanyaanId;
    public $action;
    public $button;

    protected function getRules()
    {
        $rules = ($this->action == "updatePertanyaan") ? [
            'pertanyaan.pertanyaan' => 'required' 
        ] : [
            'pertanyaan.urutan' => 'required'
        ];

        return array_merge([
            'pertanyaan.pertanyaan' => 'required|min:3',
            'pertanyaan.urutan' => 'required|min:1'
        ], $rules);
    }

    public function createPertanyaan ()
    {
        $this->resetErrorBag();
        $this->validate();

        Pertanyaan::create($this->pertanyaan);

        $this->emit('saved');
        $this->reset('pertanyaan');
    }

    public function updatePertanyaan ()
    {
        $this->resetErrorBag();
        $this->validate();

        Pertanyaan::query()
            ->where('id', $this->pertanyaanId)
            ->update([
                "pertanyaan" => $this->pertanyaan->pertanyaan,
                "urutan" => $this->pertanyaan->urutan,
            ]);

        $this->emit('saved');
    }

    public function mount ()
    {
        if (!$this->pertanyaan && $this->pertanyaanId) {
            $this->pertanyaan = Pertanyaan::find($this->pertanyaanId);
        }

        $this->button = create_button($this->action, "Pertanyaan");
    }

    public function render()
    {
        return view('livewire.create-pertanyaan');
    }
}
