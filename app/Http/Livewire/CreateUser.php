<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Illuminate\Support\Facades\DB;

class CreateUser extends Component
{
    public $user;
    public $userId;
    public $action;
    public $button;
    public $comboSatker;

    protected function getRules()
    {
        $rules = ($this->action == "updateUser") ? [
            'user.email' => 'required|email|unique:users,email,' . $this->userId,
            'user.username' => 'required|min:3',
            'user.roles' => 'required|min:3',
            'user.satker_id' => '',
        ] : [
            'user.password' => 'required|min:8|confirmed',
            'user.password_confirmation' => 'required' // livewire need this
        ];

        return array_merge([
            'user.name' => 'required|min:3',
            'user.email' => 'required|email|unique:users,email',
        ], $rules);
    }

    public function getComboSatker() {
        $data = DB::table('satkers')
            ->select("id", "nama_satker");
        $result = $data->get();
        $this->comboSatker = $result;
    }

    public function createUser ()
    {
        $this->resetErrorBag();
        $this->validate();

        $password = $this->user['password'];

        if ( !empty($password) ) {
            $this->user['password'] = Hash::make($password);
        }

        User::create($this->user);

        $this->emit('saved');
        $this->reset('user');
        $this->getComboSatker();
    }

    public function updateUser ()
    {
        $this->resetErrorBag();
        $this->validate();

        User::query()
            ->where('id', $this->userId)
            ->update([
                "name" => $this->user->name,
                "email" => $this->user->email,
                "username" => $this->user->username,
                "roles" => $this->user->roles,
                "satker_id" => $this->user->satker_id,
            ]);

        $this->emit('saved');
        $this->getComboSatker();
        // return redirect()->to('/user');
    }

    public function mount ()
    {
        if (!$this->user && $this->userId) {
            $this->user = User::find($this->userId);
        }

        $this->button = create_button($this->action, "User");
        $this->getComboSatker();
    }

    public function render()
    {
        return view('livewire.create-user');
    }
}
