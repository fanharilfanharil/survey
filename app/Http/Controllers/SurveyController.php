<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Jawaban;
use App\Models\JawabanDetail;

class SurveyController extends Controller
{
    public function index ($satker)
    {
        $search = str_replace("-"," ",$satker);
        $data = DB::table('satkers')
            ->select("id", "nama_satker")
            ->where('nama_satker', 'like', '%'.strtoupper($search).'%');
        $dataSatker = $data->first();
        $data = array();
        $data['satker'] = $dataSatker;
        return view('frontoffice.index', $data);
    }

    public function surveyWithName($satker, $responden)
    {
        $search = str_replace("-"," ",$satker);
        $data = DB::table('satkers')
            ->select("id", "nama_satker")
            ->where('nama_satker', 'like', '%'.strtoupper($search).'%');
        $dataSatker = $data->first();
        $data = array();
        $data['satker'] = $dataSatker;
        $data['name_renponden'] = $responden;
        return view('frontoffice.indexWithName', $data);
    }

    public function getPertanyaan() {
        $data = DB::table('pertanyaans')
            ->select("id", "pertanyaan")
            ->orderBy('urutan', 'asc');
        $dataSatker = $data->get();

        echo json_encode($dataSatker);
    }

    public function saveAnswer(Request $request){
        $nameResponden = $request->name_reponse;
        $idSakter = $request->id_satker;
        $dataAnswer = $request->data_answer;

        $saveAnswer = new Jawaban;
        $saveAnswer->jawaban_dari = $nameResponden;
        $saveAnswer->satker_id = $idSakter;
        $result = $saveAnswer->save();
        $idAnswer = $saveAnswer->id;

        if(!empty($dataAnswer)) {
            foreach($dataAnswer as $val) {
                $saveDetailAnswer = new JawabanDetail;
                $saveDetailAnswer->id_jawaban = $idAnswer;
                $saveDetailAnswer->pertanyaan_id = $val['id'];
                $saveDetailAnswer->jawaban = $val['value'];
                $result = $saveDetailAnswer->save();
            }
        }

        echo json_encode($result);
    }
}
