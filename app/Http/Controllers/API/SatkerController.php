<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\Satker;
use Illuminate\Http\Request;

class SatkerController extends Controller
{
    public function all(Request $request)
    {
        $id = $request->input('id');
        $limit = $request->input('limit', 5);
        $nama_satker = $request->input('nama_satker');
        $parent_satker = $request->input('parent_satker');
        
        if($id)
        {
            $satker = Satker::find($id);

            if($satker)
            {
                return ResponseFormatter::success(
                    $satker,
                    'Data Satker Berhasil Diambil'
                );
            }
            else
            {
                return ResponseFormatter::error(
                    null,
                    'Data Satker Tidak Ada',
                    404
                );
            }
        }

        $satker = Satker::query();

        if($nama_satker)
        {
            $satker->where('nama_satker','like','%'. $nama_satker . '%');
        }

        if($parent_satker)
        {
            $satker->where('parent_satker','=', $parent_satker);
        }

        return ResponseFormatter::success(
            $satker->paginate($limit),
            'Data Satker Berhasil Diambil'
        );
    }

    public function tambah(Request $request)
    {
        try {
            $request->validate([
                'parent_satker' => ['required','integer','max:10'],
                'nama_satker' => ['required','string','unique:satkers','max:255'],
            ]);

            Satker::create([
                'nama_satker' => $request->nama_satker,
                'parent_satker' => $request->parent_satker,
            ]);
            $satker = satker::where('nama_satker', $request->nama_satker)->first();
            return ResponseFormatter::success([
                'satker' => $satker
            ]);
        } catch(Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong cuyy',
                'error' => $error
            ], 'Tambah Data Gagal', 500);
        }
    }
}
