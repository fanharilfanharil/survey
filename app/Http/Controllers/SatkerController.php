<?php

namespace App\Http\Controllers;

use App\Models\Satker;
use Illuminate\Http\Request;

class SatkerController extends Controller
{
    public function index ()
    {
        return view('pages.satker.index', [
            'satker' => Satker::class
        ]);
    }

    
}
