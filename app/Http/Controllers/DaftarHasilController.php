<?php

namespace App\Http\Controllers;

use App\Models\DaftarHasil;
use App\Models\Satker;
use Illuminate\Http\Request;

class DaftarHasilController extends Controller
{
    public function index ()
    {
        return view('pages.daftarhasil.index', [
            'satker' => Satker::class
        ]);
    }

    
}
