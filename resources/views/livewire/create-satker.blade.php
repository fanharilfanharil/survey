<div id="form-create">
    <x-jet-form-section :submit="$action" class="mb-4">
        <x-slot name="title">
            {{ __('Satuan Kerja') }}
        </x-slot>

        <x-slot name="description">
            {{ __('Lengkapi data berikut dan submit untuk membuat data satker baru') }}
        </x-slot>

        <x-slot name="form">
            <div class="form-group col-span-6 sm:col-span-5">
                <x-jet-label for="nama_satker" value="{{ __('Nama Satker') }}" />
                <small>Masukkan Nama Satuan Kerja</small>
                <x-jet-input id="nama_satker" type="text" class="mt-1 block w-full form-control shadow-none" wire:model.defer="satker.nama_satker" />
                <x-jet-input-error for="satker.nama_satker" class="mt-2" />
            </div>

            <div class="form-group col-span-6 sm:col-span-5" wire:ignore>
                <x-jet-label for="parent_satker" value="{{ __('Parent Satker') }}" />
                <select id="parent_satker" class="mt-1 block w-full form-control shadow-none" wire:model.defer="satker.parent_satker">
                    <option value="0" >--Pilih--</option>
                    @foreach($comboSatker as $val)
                        <option value="{{ $val->id }}">{{ $val->nama_satker }}</option>
                    @endforeach
                </select>
                <x-jet-input-error for="satker.parent_satker" class="mt-2" />
            </div>

            <div class="form-group col-span-6 sm:col-span-5" wire:ignore>
                <x-jet-label for="tipe_satker" value="{{ __('Tipe Satker') }}" />
                <select id="tipe_satker" class="mt-1 block w-full form-control shadow-none" wire:model.defer="satker.tipe_satker">
                    <option value="0" >--Pilih--</option>
                    <option value="{{ 1 }}">{{ 'KEJAKSAAN AGUNG' }}</option>
                    <option value="{{ 2 }}">{{ 'KEJAKSAAN TINGGI' }}</option>
                    <option value="{{ 3 }}">{{ 'KEJAKSAAN NEGERI' }}</option>
                </select>
                <x-jet-input-error for="satker.tipe_sakker" class="mt-2" />
            </div>

        </x-slot>

        <x-slot name="actions">
            <x-jet-action-message class="mr-3" on="saved">
                {{ __($button['submit_response']) }}
            </x-jet-action-message>

            <x-jet-button>
                {{ __($button['submit_text']) }}
            </x-jet-button>
        </x-slot>
    </x-jet-form-section>

    <x-notify-message on="saved" type="success" :message="__($button['submit_response_notyf'])" />
</div>
