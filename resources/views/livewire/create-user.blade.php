<div id="form-create">
    <x-jet-form-section :submit="$action" class="mb-4">
        <x-slot name="title">
            {{ __('User') }}
        </x-slot>

        <x-slot name="description">
            {{ __('Lengkapi data berikut dan submit untuk membuat data user baru') }}
        </x-slot>

        <x-slot name="form">
            <div class="form-group col-span-6 sm:col-span-5">
                <x-jet-label for="name" value="{{ __('Nama') }}" />
                <small>Nama Lengkap Akun</small>
                <x-jet-input id="name" type="text" class="mt-1 block w-full form-control shadow-none" wire:model.defer="user.name" />
                <x-jet-input-error for="user.name" class="mt-2" />
            </div>

            <div class="form-group col-span-6 sm:col-span-5">
                <x-jet-label for="username" value="{{ __('Username') }}" />
                <x-jet-input id="username" type="text" class="mt-1 block w-full form-control shadow-none" wire:model.defer="user.username" />
                <x-jet-input-error for="user.username" class="mt-2" />
            </div>

            <div class="form-group col-span-6 sm:col-span-5">
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" type="text" class="mt-1 block w-full form-control shadow-none" wire:model.defer="user.email" />
                <x-jet-input-error for="user.email" class="mt-2" />
            </div>

            <div class="form-group col-span-6 sm:col-span-5" wire:ignore>
                <x-jet-label for="roles" value="{{ __('Roles') }}" />
                <select id="roles" class="mt-1 block w-full form-control shadow-none" name="roles" wire:model.defer="user.roles">
                    <option value="">--Pilih--</option>
                    <option value="Admin">Admin</option>
                    <option value="Satker">Satker</option>
                </select>
                <x-jet-input-error for="user.roles" class="mt-2" />
            </div>
            
            <div class="form-group col-span-6 sm:col-span-5" wire:ignore>
                <x-jet-label for="satker_id" value="{{ __('Satker') }}" />
                <select id="satker_id" class="mt-1 block w-full form-control shadow-none" wire:model.defer="user.satker_id">
                    <option value="0" >--Pilih--</option>
                    @foreach($comboSatker as $val)
                        <option value="{{ $val->id }}">{{ $val->nama_satker }}</option>
                    @endforeach
                </select>
                <x-jet-input-error for="user.satker_id" class="mt-2" />
            </div>

            @if ($action == "createUser")
            <div class="form-group col-span-6 sm:col-span-5">
                <x-jet-label for="password" value="{{ __('Password') }}" />
                <small>Minimal 8 karakter</small>
                <x-jet-input id="password" type="password" class="mt-1 block w-full form-control shadow-none" wire:model.defer="user.password" />
                <x-jet-input-error for="user.password" class="mt-2" />
            </div>

            <div class="form-group col-span-6 sm:col-span-5">
                <x-jet-label for="password_confirmation" value="{{ __('Konfirmasi Password') }}" />
                <small>Minimal 8 karakter</small>
                <x-jet-input id="password_confirmation" type="password" class="mt-1 block w-full form-control shadow-none" wire:model.defer="user.password_confirmation" />
                <x-jet-input-error for="user.password_confirmation" class="mt-2" />
            </div>
            @endif
        </x-slot>

        <x-slot name="actions">
            <x-jet-action-message class="mr-3" on="saved">
                {{ __($button['submit_response']) }}
            </x-jet-action-message>

            <x-jet-button>
                {{ __($button['submit_text']) }}
            </x-jet-button>
        </x-slot>
    </x-jet-form-section>

    <x-notify-message on="saved" type="success" :message="__($button['submit_response_notyf'])" />
</div>

@push('text-javascript')
<script>
    $(document).ready(function () {
        // $('#roles').select2();
        // $('#satker_id').select2();
        $('#roles').change(function(){
            var selected = $('#roles').find(":selected").val();
            if(selected == "Satker") {
                $('#satker_id').attr("disabled", false);
            }else {
                $('#satker_id').val("").trigger('change');
                $('#satker_id').attr("disabled", true);
            }
        });
    });
</script>
@endpush