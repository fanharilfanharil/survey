<x-app-layout>
    <x-slot name="header_content">
        <h1>{{ __('Daftar Hasil Survey') }}</h1>

        <div class="section-header-breadcrumb">
        <div class="breadcrumb-item"><a href="#">Survey</a></div>
            <div class="breadcrumb-item active"><a href="#">Daftar Hasil Survey</a></div>
        </div>
    </x-slot>

    <div>
        <livewire:table.main name="daftarhasil" :model="$satker" />
    </div>
</x-app-layout>
