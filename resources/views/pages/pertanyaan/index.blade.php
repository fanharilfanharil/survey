<x-app-layout>
    <x-slot name="header_content">
        <h1>{{ __('Data Pertanyaan') }}</h1>

        <div class="section-header-breadcrumb">
        <div class="breadcrumb-item"><a href="#">Data Master</a></div>
            <div class="breadcrumb-item active"><a href="#">Data Pertanyaan</a></div>
        </div>
    </x-slot>

    <div>
        <livewire:table.main name="pertanyaan" :model="$pertanyaan" />
    </div>
</x-app-layout>
