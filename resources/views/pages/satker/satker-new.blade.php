<x-app-layout>
    <x-slot name="header_content">
        <h1>{{ __('Buat Satker Baru') }}</h1>

        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item"><a href="#">Satker</a></div>
            <div class="breadcrumb-item active"><a href="#">Buat Satker Baru</a></div>
        </div>
    </x-slot>

    <div>
        <livewire:create-satker action="createSatker" />
    </div>
</x-app-layout>
